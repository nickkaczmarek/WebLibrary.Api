﻿using System.Web;
using System.Web.Http;

namespace WebLibrary.Api
{
    public class Global : HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);
        }
    }
}
