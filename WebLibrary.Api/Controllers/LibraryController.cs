﻿using System.Web.Http;
using System.Collections.Generic;
using WebLibrary.Api.Models;
using WebLibrary.Api.DAL;

namespace WebLibrary.Api.Controllers
{
    public class LibraryController : ApiController
    {
        private IBookAgent _bookAgent;

        public LibraryController(IBookAgent bookAgent)
        {
            _bookAgent = bookAgent;
        }
        public LibraryController()
        {
            _bookAgent = new BookAgent();
        }

        [Route("books/")]
        public IHttpActionResult GetAllBooks() 
        {
            return Json(_bookAgent.GetBooks());
        }
    }
}
