﻿using NUnit.Framework;
using MyTested.WebApi;
using WebLibrary.Api.Controllers;
using WebLibrary.Api.Models;
using WebLibrary.Api.DAL;
using System.Collections.Generic;
using Moq;
using AutoFixture;
using System.Linq;
using Newtonsoft.Json;

namespace WebLibrary.Api.Tests.Controllers
{
	[TestFixture]
    public class LibraryControllerTest
    {
		[Test]
        public void GetAllBooks_ShouldHaveCorrectRoute() 
        {
            MyWebApi.Routes()
                    .ShouldMap("/books")
                    .To<LibraryController>(x => x.GetAllBooks());
        }

        [Test]
        public void GetAllBooks_ReturnsJSON() 
        {
            MyWebApi.Controller<LibraryController>()
                    .Calling(x => x.GetAllBooks())
                    .ShouldReturn()
                    .Json()
                    .WithResponseModelOfType<List<Book>>();
        }

        [Test]
        public void GetAllBooks_CallsBookAgentGetBooks() 
        {
            //arrange
            var mockBookAgent = new Mock<IBookAgent>();
            var fixture = new Fixture();
            var books = fixture.CreateMany<Book>();
            mockBookAgent.Setup(x => x.GetBooks()).Returns(books.ToList());
			var controller = new LibraryController(mockBookAgent.Object);

            //act
            controller.GetAllBooks();

            //assert
            mockBookAgent.Verify(x => x.GetBooks());
        }

        [Test]
        public void GetAllBooks_ReturnsBooksList_AsJSON()
        {
            //arrange
            var mockBookAgent = new Mock<IBookAgent>();
            var fixture = new Fixture();
            var books = fixture.CreateMany<Book>();
            mockBookAgent.Setup(x => x.GetBooks()).Returns(books.ToList());
            var controller = new LibraryController(mockBookAgent.Object);
            var expectedResult = JsonConvert.SerializeObject(books);

            //act
            var actual = controller.GetAllBooks();

            //assert
            Assert.That(actual, Is.EqualTo(expectedResult));
        }
    }
}
